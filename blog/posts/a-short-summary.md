A short summary
--------
A. Federated E-commerce? Create an ecommerce website with the goal of
letting users buy and sell goods, integrate logistics into the system.

B. Cryptocurrency? Money for the internet and general use. An easy to
transfer commodity that can be readily exchanged for other goods and
services.

C. Distributed Computing? Ownership of computing and data. Computers
no longer form peers or networks, but markets. Exchanging resources
like computing power, and data, and even digital forms of other
assets, working according to the instructions of their respective
owners.

A. & B. are for human <—> human interactions.
C. is for machine <—> machine.

What kind of interactions? Interactions like negotiations, exchange
and trade analogous to the economy or the market.
