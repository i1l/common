Artificial scarcity is not scarcity
--------

Don't need it. Eventually, artificial scarcity will die. 

There is a huge demand for cryptocurrency. Still, if I insist on
artificially limiting the supply, you will have no other option but to
choose to use a different provider of cryptocurrency, and there will
be ever more players to provide one to you for less cost than me and
others.

Then the actual tangible features like market adoption, transaction
fee, transaction time will factor in much more. Etherium might be off
to a good start, the dapps could serve as a mechanism to boost
adoption beyond just being a currency.
