Prosperity
--------
How does one arrive at prosperity? I submit that prosperity arises
from a free and voluntary exchange of goods and services, all products
of specialised labor.  In a free market, prices send the correct
signals to the individuals to make decisions on what should be
produced. From “What should i make for dinner today?” to “Should I
re-locate my business?”, all can be provided very good answers for, if
the prices in the market reflect the reality.

In everyday life, we miss out on the benefits offered by this model of
society. This had been a consequence of not having the medium to set
such prices. However, that can now be changed only if we wish for
it. With innovation in money and currency, the social realms which had
not progressed at all will see great progress, atleast for those who
wish to accept such changes.

Specialisation will create efficiency and trade will distribute the
benefits far and wide.
