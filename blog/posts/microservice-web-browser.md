Break down the Monolithic Web Browser
--------

Modern Web Browsers think of themselves as the “Operating System” of
the internet. Whether the browser runs on Windows, Linux or some
mobile device is a minor detail. Browsers are also developed by small
unified teams. Working on similar ideas or the same language. By not
allowing the use of Python, or GCC they shoot themselves in the foot,
and have to redo all the work of building new programming languages
and features. Someone willing to accept that there could not be a
single monolithic program to do all of this could move much faster and
overtake the web browser duopoly by building on what's already there.

It will be cool to have support for new protocols, and experimental
features. The web browser is stagnating otherwise. The web could see
tremendous innovation from such an experimental platform.
