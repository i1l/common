Moving forward
--------
What would I do if cryptocurrency was banned? What if regulations
prohibited peer to peer trade? Answering these questions takes time
away from current problems. The only way to move forward is to assume
that solutions will exist once such problems arise, since there would
an incentive to fix them. Restrictions would only push people towards
more resistant alternatives.

Who would handle logistics? Someone would develop a peer-to-peer
logistics system for delivering goods. What if cryptocurrency was
banned? Money that cannot be censored or blocked would be
invented. What if servers were shut down? Distributed computing,
server-less computation that cannot be taken offline would be
developed.
