Go make a cryptocurrency, really!
--------
Print Money! No, really! Make your very own currency.

This will be the last time that amateurs will be able to build their
own currencies and gain any reasonable userbase before the market
settles on a 'standard' and then I as an innovator will have to
support that 'standard' currency through conversions to/from that
currency.

Money had been in-accessible to innovators for a long time. There is a
sharp demand for a easy to use, censorship resistant currency that
treats it's users right. Look at what people are willing to pay for
some of these digital assets. The crypto market is in a bubble,
because of a mix of inflation and demand for 'good money', and in a
market it's impossible to force a wedge between the two.  There is a
very high demand for sound digital money but not remotely enough
supply of it.

Some of the things I might want to consider are:
1. Starting from a blank slate.
2. Supporting niche markets, markets that otherwise would not be able
to use regular currencies.
3. Micro-transactions.
4. Offline usage.
