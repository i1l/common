require 'telegram_bot'
require 'net/http'
require 'json'
require 'securerandom'

def create_agent
    uri = URI('https://xopow.cyka.cf/products')
    http = Net::HTTP.new(uri.host, uri.port)
    http.use_ssl = true
    req = Net::HTTP::Post.new(uri.path, 'Content-Type' => 'application/json')
    req.body = {id: SecureRandom.alphanumeric, name: 'Yellow Cake', stock: 100, price: 2.35, shortDesc: 'Tastiest cake youll ever have', description: 'in-edible'}.to_json
    res = http.request(req)
    puts "response #{res.body}"
rescue => e
    puts "failed #{e}"
end

token = '<insert token here>'

bot = TelegramBot.new(token: token)

bot.get_updates(fail_silently: true) do |message|
  puts "@#{message.from.username}: #{message.text}"
  command = message.get_command_for(bot)

  message.reply do |reply|
    case command
    when /start/i
      reply.text = "All I can do is say hello. Try the /greet command."
    when /greet/i
      reply.text = "Hello, #{message.from.first_name}, :)"
    when /addyc/i
      reply.text = "Added product yellow cake to https://shop.play.ai :)"
      create_agent
    else
      reply.text = "I have no idea what #{command.inspect} means."
    end
    puts "sending #{reply.text.inspect} to @#{message.from.username}"
    reply.send_with(bot)
  end
end

